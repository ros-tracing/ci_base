# ci-base

[![pipeline status](https://gitlab.com/ros-tracing/ci_base/badges/main/pipeline.svg)](https://gitlab.com/ros-tracing/ci_base/commits/main)

Support for continuous integration for the ROS 2 tracing projects: [`ros2_tracing`](https://gitlab.com/ros-tracing/ros2_tracing) and [`tracetools_analysis`](https://gitlab.com/ros-tracing/tracetools_analysis).

## Images

There are two types of images:

* base images, which contain the usual dependencies for ROS 2
* tracing images, which are based on `:base` images and contain all the necessary dependencies for tracing (e.g. LTTng)

## Tags

| **ROS 2 distro** | **base image** | **tracing image** |
|------------------|:--------------:|:-----------------:|
| rolling          | [`:rolling-base`, `:base`](./rolling/base/Dockerfile) | [`:rolling`, `:latest`](./rolling/tracing/Dockerfile) |
| humble           | [`:humble-base`](./humble/base/Dockerfile) | [`:humble`](./humble/tracing/Dockerfile) |
| galactic         | [`:galactic-base`](./galactic/base/Dockerfile) | [`:galactic`](./galactic/tracing/Dockerfile) |
| foxy             | [`:foxy-base`](./foxy/base/Dockerfile) | [`:foxy`](./foxy/tracing/Dockerfile) |
